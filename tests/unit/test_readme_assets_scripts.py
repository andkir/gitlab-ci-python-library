import gcip
from tests import conftest
from gcip.addons.gitlab.scripts import clone_repository


def test():
    pipeline = gcip.Pipeline()
    pipeline.add_children(gcip.Job(stage="print_date", script=clone_repository("path/to/group")))

    conftest.check(pipeline.render())
